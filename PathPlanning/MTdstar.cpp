#include <stdio.h>
#include <iostream>
#include <stdlib.h>     /* calloc, exit, free */
#include <math.h>
#include <vector>

#include "MTdstar.h"
#include "gridworld.h"

static int gDir = 0;

MTDStar::MTDStar(int rows, int cols, int heuristic):SearchAlgorithm(rows, cols, heuristic)
{
    _algorithm = MTDSTAR_ALGORITHM;
}

void MTDStar::Initialise(int startX, int startY, int goalX, int goalY)
{
    SearchAlgorithm::Initialise(startX, startY, goalX, goalY);

    _pLast = _pStart;

    _km = 0.f;

	_pGoal->rhs = 0.f;

	UpdateHValues();

	calcKey(_pGoal);

	_queue.Insert(_pGoal);

    srand( time(NULL) );
    ///_nextPos = rand() % DIRECTIONS;
    _nextPos = gDir;

    _totalMoves = 0;
}

bool MTDStar::ComputeShortestPath()
{
	DBG_PRINT("[MTD*][ComPath]\t Start \n");
	bool ret = false;
    startTimer();
    initStat();

	while(true) {
		calcKey(_pStart);

		Key kOld;
		if(_queue.TopKey(kOld) == false) {
			PRINT("[MTD*][ComPath]\t There is no item to handle even though it could not find the path \n");
			ret = false;
			break;
		}

		if((kOld < _pStart->key) || (_pStart->rhs != _pStart->g)) {
            UnitCell* pTop = _queue.Pop();
            calcKey(pTop);
			DBG_PRINT("[MTD*][ComPath]\t Top priority cell \n");
			pTop->Print();

			if(kOld < pTop->key) {
				DBG_PRINT("[MTD*][ComPath]\t The kOld value of top vertex is less than calculated new key. Insert again to Queue \n");
                _queue.Insert(pTop);
			}
			else if(pTop->g > pTop->rhs) {
				DBG_PRINT("[MTD*][ComPath]\t The g value of top vertex is greater than rhs. So set rhs to g \n");
				pTop->g = pTop->rhs;
				updatePredecessor(pTop, false);
			}
			else {
				DBG_PRINT("[MTD*][ComPath]\t The g value of top vertex is less than rhs. So set INF to g \n");
				pTop->g = INF;
				updatePredecessor(pTop, true);
			}

            _numStateExpansion++;
		}
		else {
			DBG_PRINT("[MTD*][ComPath]\t Processes are successfully completed. \n");
            endTimer();
			markShortestPath();
			ret = true;
			break;
		}

		_queue.Print();
	}

	///if(_numStateExpansion)
    ///    printStat();
	return ret;
}

UnitCell* MTDStar::moveHunter()
{
    /// Move to Sstart
    _pStart->type = '0';
    /// move to next position
    _pStart = findMinSucc(_pStart);
    _pStart->type = '6';
}

UnitCell* MTDStar::moveTarget()
{
    /// Move goal to a random position
    UnitCell* pNeighbor = _pGoal->move[_nextPos];

    /// If the type of next position is traversable, moves the goal to next position
    if(pNeighbor->type == '0' || pNeighbor->type == '8') {
        _pGoal->type = '0';
        /// move to next position
        _pGoal = pNeighbor;
        _pGoal->type = '7';
    }
    else {
        ///_nextPos = rand() % DIRECTIONS;
        _nextPos = (gDir++) % DIRECTIONS;
    }

    return _pGoal;
}

bool MTDStar::basicDeletion(UnitCell * pNewGoal, UnitCell* pOldGoal)
{
    bool isGoalChanged = false;

    if(pNewGoal != pOldGoal){
        /// Basic Deletion
        if(pNewGoal->isInShortestPath == false)
        {
            isGoalChanged = true;
        }
        updateVertex(pOldGoal);
    }

    return isGoalChanged;
}

void MTDStar::updateSubTrees(UnitCell* pCell)
{
    for(int i = 0; i < DIRECTIONS; i++) {
        UnitCell* pNeighbor = pCell->move[i];
        if(pNeighbor != nullptr && pNeighbor->par == pCell) {
            updateSubTrees(pNeighbor);
        }
    }

    pCell->par = nullptr;
    pCell->rhs = INF;
    pCell->g = INF;
    _deleted.push_back(pCell);
}

bool MTDStar::optimizedDeletion(UnitCell * pNewGoal, UnitCell* pOldGoal)
{
    bool isGoalChanged = false;

    if(pNewGoal != pOldGoal)
    {
        if(pNewGoal->isInShortestPath == false)
            isGoalChanged = true;

        pNewGoal->par = nullptr;
        _deleted.clear();

        updateSubTrees(pOldGoal);

        for(int i = 0; i < _deleted.size(); i++)
        {
            UnitCell *pCell = _deleted[i];
            updateVertex(pCell);
        }
    }

    return isGoalChanged;
}

bool MTDStar::Move()
{
    bool isEdgeCostChanged = false;
    bool isGoalChanged = false;

    if(_pStart->g == INF){
        PRINT("[MTD*][Move]\t There is no known path. \n");
        return false;
    }

    UnitCell* pOldStart = _pStart;
    UnitCell* pOldGoal = _pGoal;
    UnitCell* pNewStart = moveHunter();
    _totalMoves++;
    if(_pStart == _pGoal){
        PRINT("[MTD*][Move]\t Already arrived at the goal \n");
        PRINT("\t The total number of state expansions : %d \n", _totalStateExpansion);
        PRINT("\t Actual running time (sec.) : %.3f \n", ((float)_totalElapsedTime)/CLOCKS_PER_SEC);
        PRINT("\t Total moves : %d \n", _totalMoves);
        return false;
    }

    UnitCell* pNewGoal = moveTarget();

    isGoalChanged = optimizedDeletion(pNewGoal, pOldGoal);
    //isGoalChanged = basicDeletion(pNewGoal, pOldGoal);

    isEdgeCostChanged = updateEdgeCosts(_pStart);
    if(isEdgeCostChanged || isGoalChanged){
        ComputeShortestPath();
    }

    return true;
}

void MTDStar::UpdateAllKeyValues()
{
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
		   calcKey(&_maze[i][j]);
		}
	}
}

void MTDStar::UpdateHValues()
{
	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
		   _maze[i][j].h = calc_H(j, i);
		}
	}
}
//================================= Protected methods =======================================
void MTDStar::updatePredecessor(UnitCell* pCell, bool updateItself)
{
	DBG_PRINT("[MTD*][UdtVtx]\t update all successors %s of vertex (%d, %d) \n", (updateItself) ? "including vertex itself":"" , pCell->x, pCell->y);

    for(int i = 0; i < DIRECTIONS; i++){
        if((pCell->move[i] != nullptr) && (pCell->linkCost[i] != INF))
            updateVertex(pCell->move[i]);
    }

    if(updateItself)
        updateVertex(pCell);
}

void MTDStar::updateVertex(UnitCell* pCell)
{
	DBG_PRINT("[MTD*][Udt %d,%d]\t update \n", pCell->x, pCell->y);

	if(pCell != _pGoal){
        int minNeighbor = 0;
		pCell->rhs = calcRhs(pCell, &minNeighbor);
        pCell->par = pCell->move[minNeighbor];
		DBG_PRINT("[MTD*][Udt %d,%d]\t vertex is not start vertex and calculated min rhs %.1f \n", pCell->x, pCell->y, pCell->rhs);
	}

	QueueIt it = _queue.Find(pCell);
	if(_queue.IsEnd(it) == false) {
		DBG_PRINT("[MTD*][Udt %d,%d]\t vertex is in the Queue. So erase it from queue \n", pCell->x, pCell->y);
		_queue.Remove(it);
	}

	if(pCell->g != pCell->rhs) {
		DBG_PRINT("[MTD*][Udt %d,%d]\t g(%.1f) of vertex is diff with rhs(%.1f). So calculates and insert again \n", pCell->x, pCell->y, pCell->g, pCell->rhs);
		calcKey(pCell);
		_queue.Insert(pCell);
	}

	if(pCell->rhs >= INF)
        pCell->par = nullptr;
}

bool MTDStar::updateEdgeCosts(UnitCell* pCell)
{
    bool updated = false;
    for(int i = 0; i < DIRECTIONS; i++){
        UnitCell* pNeighbor = pCell->move[i];
        if((pNeighbor != nullptr) && (pNeighbor->type == '9' || pNeighbor->type == '8')) {
            if(updated == false) {
                _km += calcDistance(_pLast->x, _pLast->y, _pStart->x, _pStart->y);
                _pLast = _pStart;
                updated = true;
            }

            char type = (pNeighbor->type == '9') ? '1' : '0';
            UpdateCellType(pNeighbor->x, pNeighbor->y, type);
        }
    }

    return updated;
}

UnitCell* MTDStar::findMinSucc(UnitCell* pCell)
{
	double      minRhs = INF;
	UnitCell*   pMinNeighbor = nullptr;

	for(int i = 0; i < DIRECTIONS; i++) {
	    UnitCell* pNeighbor = pCell->move[i];
		if(pNeighbor != nullptr) {
            double cost = pNeighbor->g + pCell->linkCost[i];
			if(cost < minRhs) {
				minRhs = cost;
				pMinNeighbor = pNeighbor;
			}
		}
	}

    return pMinNeighbor;
}

double MTDStar::calcRhs(UnitCell* pCell, int* minNeighbor)
{
	double minRhs = INF;

	for(int i = 0; i < DIRECTIONS; i++) {
	    UnitCell* pNeighbor = pCell->move[i];
		if(pNeighbor != nullptr) {
            double cost = pNeighbor->g + pCell->linkCost[i];
			if(cost < minRhs){
				minRhs = cost;
				*minNeighbor = i;
			}
		}
	}

	DBG_PRINT("[MTD*][CalRhs]\t calculated min rhs value is %.1f \n", minRhs);
	return minRhs;
}

double MTDStar::minValue(double g_, double rhs_)
{
	if(g_ <= rhs_){
		return g_;
	} else {
		return rhs_;
	}
}

double MTDStar::calc_H(int x, int y)
{
    return calcDistance(_pStart->x, _pStart->y, x, y);
}

void MTDStar::calcKey(int x, int y)
{
	calcKey(&_maze[y][x]);
}

void MTDStar::calcKey(UnitCell *cell)
{
	double key1, key2;

	key2 = minValue(cell->g, cell->rhs);
	key1 = key2 + cell->h + _km;

	cell->key.val[0] = key1;
	cell->key.val[1] = key2;
}

