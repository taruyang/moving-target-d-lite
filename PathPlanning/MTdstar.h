#ifndef __MTDSTAR_H__
#define __MTDSTAR_H__

#include <vector>
#include <set>
#include "globalVariables.h"
#include "SearchAlgorithm.h"


class GridWorld; //forward declare class GridWorld to be able to create the friend functions later

class MTDStar: public SearchAlgorithm {

public:
    MTDStar(int rows, int cols, int heuristic = MANHATTAN); //constructor

    void Initialise(int startX, int startY, int goalX, int goalY);
    bool ComputeShortestPath();
    void UpdateHValues();
    void UpdateAllKeyValues();
    bool Move();

protected:
    void updatePredecessor(UnitCell* pCell, bool updateItself);
    void updateVertex(UnitCell* pCell);
    bool updateEdgeCosts(UnitCell* pCell);

    bool basicDeletion(UnitCell * pNewGoal, UnitCell* pOldGoal);
    bool optimizedDeletion(UnitCell * pNewGoal, UnitCell* pOldGoal);
    void updateSubTrees(UnitCell* pCell);

    UnitCell* findMinSucc(UnitCell* pCell);
    double calcRhs(UnitCell* center, int* minNeighbor);
	double minValue(double g_, double rhs_);
	void calcKey(int x, int y);
    void calcKey(UnitCell* pCell);
    double calc_H(int x, int y);
    UnitCell* moveHunter();
    UnitCell* moveTarget();

protected:
    UnitCell* _pLast;
    int _nextPos;
    double _km;
    vector<UnitCell*> _deleted;
    int _totalMoves;
};



#endif
