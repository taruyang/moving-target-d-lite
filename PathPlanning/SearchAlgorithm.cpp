#include <cstdlib>
#include "SearchAlgorithm.h"

 SearchAlgorithm::SearchAlgorithm(int rows, int cols, int heuristic)
 {
    _rows = rows;
    _cols = cols;
    _heuristic = heuristic;
    _numStateExpansion = 0;

    ///Allocate memory
    _maze.resize(_rows);
    for(int i=0; i < _rows; i++)
        _maze[i].resize(_cols);

    initTimer();
}

void SearchAlgorithm::Initialise(int startX, int startY, int goalX, int goalY)
{
	DBG_PRINT("[SC][Init]\t Maze Init \n");

	for(int i=0; i < _rows; i++){
	   for(int j=0; j < _cols; j++){
			_maze[i][j].g = INF;
			_maze[i][j].rhs = INF;
			_maze[i][j].isInShortestPath = false;
			_maze[i][j].par = nullptr;

            for(int m=0; m < DIRECTIONS; m++){
                int neighbourY = i + neighbours[m].y;
                int neighbourX = j + neighbours[m].x;

                if((neighbourX >= 0) && (neighbourX < _cols) && (neighbourY >= 0) && (neighbourY < _rows))
                    _maze[i][j].move[m] = &_maze[neighbourY][neighbourX];
            }
		}
	}

	_pStart = &_maze[startY][startX];
	_pGoal = &_maze[goalY][goalX];

	///START VERTEX
	_pStart->x = startX;
	_pStart->y = startY;

	///GOAL VERTEX
	_pGoal->x = goalX;
	_pGoal->y = goalY;

    _totalStateExpansion = 0;
    _totalElapsedTime = 0;
}

void SearchAlgorithm::UpdateCellType(int x, int y, char type)
{
	DBG_PRINT("[SC][Ch %d, %d]\t Change type from to %d \n", x, y, _maze[y][x].type, type);

    if(_maze[y][x].type == type)
        return;

    UnitCell* pCell = &_maze[y][x];
    pCell->type = type;

    for(int m = 0; m < DIRECTIONS; m++){
        double cost = (type == '1') ? INF : ((abs(neighbours[m].y) + abs(neighbours[m].x)) < 2) ? 1.f : SQRT_2;;

        UnitCell* pNeighbour = pCell->move[m];
        if((pNeighbour != nullptr) && (pNeighbour->type != '1')) {
            pNeighbour->linkCost[DIRECTIONS - 1 - m] = cost;
            updateVertex(pNeighbour);
            pCell->linkCost[m] = cost;
        }
    }
}

void SearchAlgorithm::printStat()
{
    PRINT("[SC][Print] ==================================================\n");
    PRINT("\t Maximum queue length : %d \n", _queue.GetMaxQLen());
    PRINT("\t The number of state expansions : %d \n", _numStateExpansion);
    PRINT("\t The number of vertex access : %lu \n", _queue.GetNumVertexAssess());
    PRINT("\t Actual running time (sec.) : %.3f \n", ((float)_elapsedTime)/CLOCKS_PER_SEC);
    PRINT("[SC][Print] ==================================================\n");
}

void SearchAlgorithm::initStat()
{
    _numStateExpansion = 0;
    _elapsedTime = 0;
    _queue.InitStat();
}

double SearchAlgorithm::calcDistance(int x1, int y1, int x2, int y2)
{
    int diffY = abs(y1 - y2);
    int diffX = abs(x1 - x2);
    double distance = 0.f;

    if(_heuristic == MANHATTAN) {
        distance = (diffY >= diffX) ? (double)diffY : (double)diffX;
    }
    else {
        distance = sqrt(diffY * diffY + diffX * diffX);
    }

    return distance;
}

double SearchAlgorithm::calcPathLen()
{
    UnitCell* originVertex;
    UnitCell* neighbour;
    UnitCell* minNeighbour;
    double linkCost = 0.f, g = 0.f;
    double minLinkCost = 0.f;
    double totalPath = 0.f;
    double minGplusC = INF;
    double gPlusC = 0.f;
    originVertex = (_algorithm == LPASTAR_ALGORITHM) ? _pGoal : _pStart;

    while(1){
        minGplusC = INF;

        /// find neighbour with min (g+c), set as min_neighbour
        for(int m=0; m < DIRECTIONS; m++){
            neighbour = originVertex->move[m];
            if(neighbour != NULL && neighbour->type != '1'){
                linkCost = originVertex->linkCost[m];
                g = (originVertex->move[m])->g;
                gPlusC = linkCost + g;

                if(minGplusC > gPlusC){
                    minGplusC = gPlusC;
                    minLinkCost = linkCost;
                    minNeighbour = neighbour;
                }
            }
        }

        totalPath = totalPath + minLinkCost;
        originVertex = minNeighbour;

        if(_algorithm == LPASTAR_ALGORITHM) {
            if(originVertex == _pStart)
                break;
        }
        else {
            if(originVertex == _pGoal)
                break;
        }
    }

    return totalPath;
}

void SearchAlgorithm::markShortestPath()
{
    UnitCell* originVertex;
    UnitCell* neighbour;
    UnitCell* minNeighbour;
    double linkCost = 0.f, g = 0.f;
    double minGplusC = INF;
    double gPlusC = 0.f;
    originVertex = (_algorithm == LPASTAR_ALGORITHM) ? _pGoal : _pStart;
    originVertex->isInShortestPath = true;

    _totalStateExpansion += _numStateExpansion;
    _totalElapsedTime += _elapsedTime;

	for(int i=0; i < _rows; i++)
	   for(int j=0; j < _cols; j++)
			_maze[i][j].isInShortestPath = false;

    while(1){
        minGplusC = INF;

        /// find neighbour with min (g+c), set as min_neighbour
        for(int m=0; m < DIRECTIONS; m++){
            neighbour = originVertex->move[m];
            if(neighbour != NULL && neighbour->type != '1'){
                linkCost = originVertex->linkCost[m];
                g = (originVertex->move[m])->g;
                gPlusC = linkCost + g;

                if(minGplusC > gPlusC){
                    minGplusC = gPlusC;
                    minNeighbour = neighbour;
                }
            }
        }

        originVertex = minNeighbour;
        originVertex->isInShortestPath = true;

        if(_algorithm == LPASTAR_ALGORITHM) {
            if(originVertex == _pStart)
                break;
        }
        else {
            if(originVertex == _pGoal)
                break;
        }
    }
}
