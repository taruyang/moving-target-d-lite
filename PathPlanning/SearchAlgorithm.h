#ifndef SEARCHALGORITHM_H_INCLUDED
#define SEARCHALGORITHM_H_INCLUDED

#include <time.h>
#include "globalVariables.h"
#include "PriorityQueue.h"

class GridWorld;

class SearchAlgorithm{

public:
    SearchAlgorithm(int rows, int cols, int heuristic = MANHATTAN);
    virtual ~SearchAlgorithm(){}

    virtual void Initialise(int startX, int startY, int goalX, int goalY);
    virtual void UpdateCellType(int x, int y, char type);
    virtual bool ComputeShortestPath() = 0;
    virtual void UpdateHValues() = 0;
    virtual void UpdateAllKeyValues() = 0;
    virtual bool Move() = 0;

	friend void copyMazeToDisplayMap(GridWorld &gWorld, SearchAlgorithm* sa);
	friend void copyDisplayMapToMaze(GridWorld &gWorld, SearchAlgorithm* sa);

protected:

    virtual void updateVertex(UnitCell* pCell) = 0;

    double calcDistance(int x1, int y1, int x2, int y2);
    double calcPathLen();

    void markShortestPath();

    void initTimer(){ _time = 0; _elapsedTime = 0;}
    void startTimer() { _time = clock();}
    void endTimer() {_elapsedTime = clock() - _time;}

    void initStat();
    void printStat();

protected:
    vector<vector<UnitCell> > _maze;

    UnitCell* _pStart;
    UnitCell* _pGoal;

    int _rows;
    int _cols;
    int _heuristic;
    int _numStateExpansion;

    unsigned int _totalStateExpansion;
    clock_t _totalElapsedTime;

    clock_t _elapsedTime;
    clock_t _time;

    PathPlanning _algorithm;

    PriorityQueue _queue;
};

#endif // SEARCHALGORITHM_H_INCLUDED
